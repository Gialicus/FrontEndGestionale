import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required] );
  user: User = new User();

  constructor(
    private userService: UserService,
    private loginService: LoginService,
    private router: Router) { }

  ngOnInit() {
  }
  getErrorEmailMessage() {
    return this.username.hasError('required') ? 'You must enter a value' :
        this.username.hasError('username') ? 'Not a valid username' :
            '';
  }
  getErrorPasswordMessage() {
    return this.password.hasError('required') ? 'You must enter a value' :
        this.username.hasError('password') ? 'Not a valid password' :
            '';
  }
  provaLog() {
    this.loginService.login(this.username.value, this.password.value);
  }
  goRegistrationView() {
    this.router.navigate(['/registrazione']);
  }

}
