export class User {
    public id: number;
    public nome: string;
    public cognome: string;
    public email: string;
    public password: string;
    public codiceFiscale: string;
    public partitaIva: string;
    public telefono: string;
    public ruolo: number;
    public token: string;
    constructor() {}
}
