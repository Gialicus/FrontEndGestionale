import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  carte = [
    {title: "foto1", descrizione: "fotouno", imageUrl: "../../assets/img-massagio-1.jpg"},
    {title: "foto2", descrizione: "fotodue", imageUrl: "../../assets/img-massagio-2.jpg"},
    {title: "foto3", descrizione: "fototre", imageUrl: "../../assets/img-massagio-3.jpg"}
  ];
  constructor() { }

  ngOnInit() {
  }

}
