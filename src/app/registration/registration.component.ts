import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../services/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  user: User;

  userForm: FormGroup = this.formBuilder.group({
    nome: new FormControl('', [Validators.required]),
    cognome: new FormControl('', [Validators.required]),
    username: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    partitaIva: new FormControl('', [Validators.required]),
    telefono: new FormControl('', [Validators.required]),
    codiceFiscale: new FormControl('', [Validators.required])
  });



  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private registrationService: RegistrationService) { }

  ngOnInit() {
  }

  getErrorMessageNome() {
      return this.userForm.controls.nome.hasError('required') ? 'You must enter a value' : '';
  }
  getErrorMessageCognome() {
      return this.userForm.controls.cognome.hasError('required') ? 'You must enter a value' : '';
  }
  getErrorMessageEmail() {
      return this.userForm.controls.email.hasError('required') ? 'You must enter a value' : '';
  }
  getErrorMessagePassword() {
      return this.userForm.controls.password.hasError('required') ? 'You must enter a value' : '';
  }
  getErrorMessageUsername() {
      return this.userForm.controls.username.hasError('required') ? 'You must enter a value' : '';
  }
  getErrorMessageTelefono() {
      return this.userForm.controls.telefono.hasError('required') ? 'You must enter a value' : '';
  }
  getErrorMessageCodiceFiscale() {
      return this.userForm.controls.codiceFiscale.hasError('required') ? 'You must enter a value' : '';
  }

  doRegister() {
    this.user = Object.assign({}, this.userForm.value);
    console.log(this.user);
    this.registrationService.sendUser(this.user);
    this.router.navigate(['/login']);
  }
  goLoginView() {
    this.router.navigate(['/login']);
  }
}
