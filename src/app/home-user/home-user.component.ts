import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.css']
})
export class HomeUserComponent implements OnInit {

  user: User = new User();

  constructor(
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.user = this.userService.getUser();
    console.log('user passato da login component: ' + this.user.nome + ' ' + this.user.token);
  }

  goPrenotaAttivita() {
    this.router.navigate(['/prenotazioni']);
  }

  goVisualizzaAttivita() {
    this.router.navigate(['/visualizza']);
  }

  goImpostazioni() {
    this.router.navigate(['/impostazioni']);
  }

}
