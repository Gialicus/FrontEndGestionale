import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { UserService } from './user.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  user: User = new User();

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private userService: UserService
  ) { }

  login(username: string, password: string): void {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const params = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('grant_type', 'password');
    const options = {
      headers,
      params,
      withCredentials: true
    };
    this.httpClient.post<any>('http://localhost:8585/auth/oauth/token', null, options).toPromise().then(

      res => {
        sessionStorage.setItem('token', res.access_token);
        sessionStorage.setItem('username', username);
        console.log('AUTENTICATO');
        this.router.navigateByUrl('/home');
        this.user.nome = username;
        this.user.token = res.access_token;
        this.userService.setUser(this.user);
      }
    ).catch(() => {
      console.log('non AUTENTICATO');
      sessionStorage.removeItem('username');
      this.router.navigateByUrl('/login');
    });
  }

  isUserLoggedIn() {
    const user = sessionStorage.getItem('username');
    return !(user === null);
  }

  logOut() {
    sessionStorage.removeItem('username');
  }

}
