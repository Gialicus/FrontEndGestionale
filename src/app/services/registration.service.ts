import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  // http://localhost:8585/insertCliente'
  // http://httpbin.org/post
   userUrl = 'http://localhost:8585/login/insertCliente';
  constructor(
    private http: HttpClient,
  ) { }

  sendUser(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const options = {
      headers,
      withCredentials: true
    };
    return this.http.post<User>(this.userUrl, user, options)
    .toPromise()
    .then( data => {
      console.log('data che ritorna dal server:');
      return console.log(data);
    } )
    .catch( () => {
      console.log('errore');
    });
  }
}
