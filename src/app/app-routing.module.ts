import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { HomeUserComponent } from './home-user/home-user.component';
import { AboutComponent } from './about/about.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'about', component: AboutComponent },
  { path: 'registrazione', component: RegistrationComponent },
  { path: 'home', component: HomeUserComponent },
  { path: 'prenotazioni', component: HomeUserComponent },
  { path: 'visualizza', component: HomeUserComponent },
  { path: 'impostazioni', component: HomeUserComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
